# This function generates a possible assignment based on a number i.
#
# The function recieves three parameters:
#	- m:Int, is the base used for the conversion.
#	- n:Int, is the pad used in conversion (the size of the resulting array).
#	- i:Int, is the number to be transformed.
#
# The function returns one value:
#	- asg:Int[n], an array of n elements, where each element has a value in range [0,m-1].


function genAssignment(m::Int, n::Int, i::Int)
	# Initialization
	asg = zeros(Int, n)

	# Conversion of the number into a proper String. The value i-1 is used because possible values start at zero.
	aux = base(m, i-1, n)
	
	# Conversion of the String into a proper array of Int.
#	for j=1:n
#		asg[j] = parse(Int,aux[j]) + 1
#	end
	asg = parse.(Int,split(aux,"")).+1
	return asg
end

