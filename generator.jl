"""
	generator(ty::Char, agents::Int, jobs::Int)

Generates a file (in the same directory) that can be used as input for GAP-like algorithms.

File format is specified as in [OR-Library](http://people.brunel.ac.uk/~mastjjb/jeb/orlib/gapinfo.html)

# Arguments:
  - ty::Char, a char representing the type of benchmark to generate ('A', 'B', 'C' or 'D').
  - agents::Int, the number of agents performing the jobs. Also referred as 'm'.
  - jobs::Int, the number of jobs to be performed by the agents. Also referred as 'n'.
"""
function generator(ty::Char, agents::Int, jobs::Int)

### Generate matrices A, B and C depending on selected type
if ty == 'A'
	c = rand!(zeros(Int,agents,jobs),10:50)
	a = rand!(zeros(Int,agents,jobs),5:25)
	vals,inds = findmin(c,1)
	iMins, bar = ind2sub(size(c),inds[:])
	R = zeros(Int,agents)
	for k=1:agents
		R[k] = sum(a[k,:] .* (iMins .== k))
	end
	Rmax = maximum(R)
	b = round.(Int,ones(agents) * (0.6 * jobs / agents * 15 + 0.4 * Rmax))
elseif ty == 'B'
	c = rand!(zeros(Int,agents,jobs),10:50)
	a = rand!(zeros(Int,agents,jobs),5:25)
	vals,inds = findmin(c,1)
	iMins, bar = ind2sub(size(c),inds[:])
	R = zeros(Int,agents)
	for k=1:agents
		R[k] = sum(a[k,:] .* (iMins .== k))
	end
	Rmax = maximum(R)
	b = round.(Int,0.7 * ones(agents) * (0.6 * jobs / agents * 15 + 0.4 * Rmax))
elseif ty == 'C'
	c = rand!(zeros(Int,agents,jobs),10:50)
	a = rand!(zeros(Int,agents,jobs),5:25)
	b = round.(Int,0.8 * (sum(a,2) ./ agents))
elseif ty == 'D'
	a = rand!(zeros(Int,agents,jobs),1:100)
	e1 = rand!(zeros(Int,agents,jobs),-10:10)
	c = 111 - a + e1
	b = round.(Int,0.8 * (sum(a,2) ./ agents))
else
	warn("The selected benchmark type does not exists or is currently not supported.")
	return
end

### Write results to a text file

# file name will contain benchmark type, and row and column size, ie: "A310.txt"
filename = string(ty, agents, jobs, ".txt")
# Open the file. If the file does not exist()s, it will be created; if it does, it will be overwritten.
wfile = open(filename,"w")
# Write the first line, which contains number of agents and jobs
println(wfile, "$agents $jobs")
# Write the C and A matrix, and B vector
writedlm(wfile, c, ' ')
writedlm(wfile, a, ' ')
writedlm(wfile, b', ' ')
# close the file
close(wfile)

end