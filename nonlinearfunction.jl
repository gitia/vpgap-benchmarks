# Required modules
using Distributions


"""
	nonlinearfunction(minval::Any, maxval::Any, sigma::Any, std::Any, vector::Array{::Any})

Models a non linear function in the interval [0,...].

Below a minimum value, and over a maximum value, it returns zero.
Between min-max values, it returns a normalized gaussian, whose sigma and standard deviation are inputs.
"""
function nonlinearfunction(minval, maxval, sigma, std, vector)
	# Initialization
	hj = zeros(1,size(vector,2))
	d = Normal(sigma,std)
	
    if (all(vector .>= minval))
        if (all(vector .<= maxval)) 
            hj = pdf.(d, vector) ./ pdf(d, sigma)
        end
    end
	return hj
end

