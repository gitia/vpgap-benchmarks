"""
	loadData(file)

Reads an input data file and load the values in corresponding variables.

File format is specified as in [OR-Library](http://people.brunel.ac.uk/~mastjjb/jeb/orlib/gapinfo.html)

The function returns five values:
  * m::Int, the number of agents
  * n::Int, the number of tasks
  * a::Int[n,m], a matrix representing the capacities consumed
  * c::Int[n,m], a matrix representing the profits obtained
  * b::Int[n], an array representing the max capacity of agents
"""
function loadData(file)
	# Grab contents of input file into an auxiliar matrix
	aux = readdlm(file)

	# Obtain return values directly from auxiliar matrix 
	m = aux[1,1]
	n = aux[1,2]
	c = convert(Array{Int}, aux[2:2+m-1,:])		# The convert method is needed because default type of Julia is ::Any
	a = convert(Array{Int}, aux[2+m:2+m+m-1,:])
	b = convert(Array{Int}, aux[2+m+m,1:m])

	return m, n, a, c, b
end
