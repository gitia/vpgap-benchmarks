# Required modules
using ArgParse

"""
	parse_commandline()

Defines command line arguments for bruteforce algorithm. 
"""
function parse_commandline()
	s = ArgParseSettings()

	@add_arg_table s begin
	    "--write", "-w"
	       help = "if set, write results to a file."
	       action = :store_true
		"--output", "-o"
			help = "defines the file for writing results."
			arg_type = String
			default = "results.txt"
		"--dir", "-d"
			help = "executes all input files contained in specified directory."
			arg_type = String
	    "file(s)"
	        help = "file(s) to be processed."
			nargs = '*'
			arg_type = String
			default = [""]
	end
	
	return parse_args(s)
end
