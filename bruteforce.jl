# Pre-loading of called functions 
include("nonlinearfunction.jl")
include("loadData.jl")
include("parse_commandline.jl")

# Variables used for non linear function
const hj_mincap = 0.3            # minimum capacity for nonlinear function
const hj_maxcap = 1.0            # maximum capacity for nonlinear function
const hj_mu = 0.85               # sigma for nonlinear function
const hj_std = 0.25              # standard deviation for nonlinear function

# Read the input data file from command line
parsed_args = parse_commandline()

# Obtain lists of files to be processed
filesdir = parsed_args["dir"]
if filesdir != nothing						# if a directory is provided in command line
	files = readdir(filesdir)
	files = joinpath.(filesdir, files)
	extrafiles = parsed_args["file(s)"]		# if there are also individual input files, they are added
	if extrafiles != [""]
		files = [files; extrafiles]
	end
else
	files = parsed_args["file(s)"]			# if there are individual input files only
	if files == [""]						# and if there is no input file provided, use default one.
		files = ["tests/A312.txt"]
		warn("No input data file specified. Using default input data file (tests/A312.txt).")
	end
end

filesCount = length(files)
for f=1:filesCount
	info("Processing file $f of $filesCount \n")

	# Load data from input file.
	inputFile = files[f]
	m, n, a, c, b = loadData(inputFile)

	# Variables initialization
	assignments = zeros(m^n,n)		# Each row of this matrix is a possible assignment
	profits = zeros(m^n,m)			# Each row contains the profit of each agent for an assignment
	capacityUsed = zeros(m^n,m)		# Each row shows the agents' capacity used for an assignment
	solutionsCount = 0				# Counter of valid solutions
	validSolutions = zeros(m^n,2)	# Each row contains the Z value for a valid solution and the assignment number that generated it

	###### Main loop
	loopTime = @elapsed for i=1:m^n						# The loop generates all possible assignments
		# First it generates one assignment
		aux = base(m, i-1, n)							# Conversion of the number into a proper String. 
														# The value i-1 is used because possible values start at zero.
		for j=1:n
			assignments[i,j] = parse(Int,aux[j]) + 1	# Conversion of the String into a proper array of Int.
		end

		# Then, for each agent...
		for k=1:m

			# ... find wich jobs have been assigned to it,
			indexes = find(assignments[i,:] .== k)
			
			# calculate the capacity used, 
			capacityUsed[i,k] = sum(a[k,indexes]) / b[k]
			
			# and calculates its profit.
			profits[i,k] = sum(1 ./ c[k,indexes])
		end

		# Calculate the efficiency factor for every agent
		hj = nonlinearfunction(hj_mincap, hj_maxcap, hj_mu, hj_std, capacityUsed[i,:])

		if (all(hj .> 0))  # If the assignment is a valid solution...
			# increment the counter,
			solutionsCount = solutionsCount + 1
			# calculate the Z value, and store it into an array.
			validSolutions[solutionsCount,1] = sum(hj .* profits[i,:])
			validSolutions[solutionsCount,2] = i
		end
	end
	# End of Main loop

	if solutionsCount > 0
		optimumValue, optIndex = findmax(validSolutions[:,1])
		solIndex = Int(validSolutions[optIndex,2])
		println("File: ", inputFile)
		println("Time: ", loopTime)
		println("Solutions found: ", solutionsCount)
		println("Optimum value: ", optimumValue)
		println("Capacities: ", capacityUsed[solIndex,:])
		println("Assignment: ", assignments[solIndex,:])
		
		# Write results to a file
		if parsed_args["write"]
			wfilename = parsed_args["output"]
			str = string(inputFile, "\t", loopTime, "\t", solutionsCount, "\t", optimumValue, "\t")
			str = string(str, capacityUsed[solIndex,:], "\t", assignments[solIndex,:])
			wfile = open(wfilename,"a")
			println(wfile, str)
			close(wfile)
		end
	else
		print_with_color(:red, "No solutions found.")
	end

	# Free memory
	assignments = nothing
	validSolutions = nothing
	capacityUsed = nothing
	solutionsCount = nothing
	profits = nothing
	m = nothing
	n = nothing
	a = nothing
	b = nothing
	c = nothing
	gc()

end
