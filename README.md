# Benchmark Suite for VPGAP

## What is this repository for?

This repository consists of a set of input data tests for VPGAP algorithm, and a bruteforce algorithm written in Julia to solve them. 

VPGAP stands for Variable Profit Generalised Assignment Problem, a novel variant of the well-known GAP.
More info about GAP or this repo can be found in... (paper sent to journal).

The 20 input data files for the algorithm are located in `tests` folder. 
The format of the files is the same as in the [OR-Library](http://people.brunel.ac.uk/~mastjjb/jeb/orlib/gapinfo.html)

## Installation and set up

As the benchmarks are written in Julia language, this is a prerequisite. 
Julia can be downloaded from https://julialang.org/downloads/
The files are currently written for version 0.6.2.

We recommend adding Julia to PATH environment, as the benchmarks can recieve command line options.
Adding Julia to PATH is platform specific (it depends of the target Operating System).

Once Julia is installed, there are two Julia packages needed: [Distributions](https://juliastats.github.io/Distributions.jl/latest/index.html) and [ArgParse](http://carlobaldassi.github.io/ArgParse.jl/stable/). 
Both packages can be easy installed through Julia's package manager. Simply type in Julia's command line:

	julia> Pkg.add("Distributions")

and

	julia> Pkg.add("ArgParse")

Dependencies for both packages should be installed automatically.

## Usage

The main file is `bruteforce.jl`. 
The recommended use of the benchmark is through command line, but it also can be executed through Julia's own interface. So, typing

	julia bruteforce.jl

or 

	julia> include("bruteforce.jl")

should be identical. But we recommend first usage because of command line options.

* * *
If no parameter is passed to `bruteforce.jl`, it executes the benchmark with `tests/A312.txt` as default input data file, 
and it shows results in console.

For executing the benchmark with another input data file, simply put the input data file name after `bruteforce.jl`. For example:

	julia bruteforce.jl
Executes benchmark for default input file (`tests/A312.txt`).

	julia bruteforce.jl tests/D312.txt
Executes benchmark for D312.txt input file, located in `tests` directory.

	julia bruteforce.jl tests/C312.txt tests/D312.txt
Executes benchmark for both C312.txt and D312.txt input files, both located in `tests` directory.


Any number of input data files can be passed, and in any directory.

* * * 
For executing the benchmark for all input data files contained in a directory, one can use the `-d` or `--dir` command line option. For example:

	julia bruteforce.jl -d tests
Executes benchmark for all input files in `tests` directory.

	julia bruteforce.jl -d tests A312.txt
Executes benchmark for all input files in `tests` directory *AND* input data file A312.txt in same directory of `bruteforce.jl`.

* * * 
The benchmark can save the results to an output file. This is provided by the `-w` or `--write` coomand line option. 
If this flag is set, results are shown on console and also saved to a default file results.txt in the same directory of `bruteforce.jl`.
The output file name (or directory) can be changed using the `-o` or `--output` command line option, 
followed by a string representing the name of the output file. For example:

	julia bruteforce.jl -w tests/D312.txt
Executes benchmark for D312.txt input file, and write results to file `results.txt` in same directory as `bruteforce.jl`.

	julia bruteforce.jl -wo sample/myfile.txt -d tests
Executes benchmark for all input files in `tests` directory, and write results to file `myfile.txt` in `sample` directory.

If the output file doesn't exist, it will be created. But if it exists, results will be appended. 
This feature could be useful for keeping results of multiple executions on the same output file.


#### Usage summary

```bash
julia bruteforce.jl [-w] [-o wfilename] [-d dir] [INPUTFILES]
```

where:

- `-w, --write` If set, writes results to output file.
- `-o, --outputs=<s>` Defines the name of the output file. If no name is given, `results.txt` will be used.
- `-d, --dir=<s>` Defines a directory where the input files will be taken from.
- `INPUTFILES` Specifies one or more input files to be processed.


## Any Questions?
Feel free to contact me: nicolas.majorel at gitia.com. 
